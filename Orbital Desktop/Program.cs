﻿using Microsoft.Win32;
using Orbital_Desktop.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Orbital_Desktop
{
    public class OrbitalDesktop : ApplicationContext
    {
        Timer timer = new Timer();
        const int SPI_SETDESKWALLPAPER = 20;
        const int SPIF_UPDATEINIFILE = 0x01;
        const int SPIF_SENDWININICHANGE = 0x02;
        const int SPIF_SENDCHANGE = 0x02;
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);
        public delegate int HookProc(int nCode, IntPtr wParam, IntPtr lParam);
        
        static int hHook = 0;
        static int hHookadv = 0;
        public const int WH_MOUSE = 7;
        public const int WH_MOUSE_LL = 14;
        public const int WM_MOUSEHWHEEL = 0x020E;
        HookProc MouseHookProcedure;
        HookProc AdvMouseHookProcedure;
        [StructLayout(LayoutKind.Sequential)]
        public class POINT
        {
            public int x;
            public int y;
        }
        
        [StructLayout(LayoutKind.Sequential)]
        public class MouseHookStruct
        {
            public POINT pt;
            public int hwnd;
            public int wHitTestCode;
            public int dwExtraInfo;
        }
        [StructLayout(LayoutKind.Sequential)]
        public class tagMSLLHOOKSTRUCT
        {
            public POINT pt;
            public int mouseData;
            public int flags;
            public int time;
            public int dwExtraInfo;
        }
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Winapi)]
        public extern static int GetDesktopWindow();
        [DllImport("user32.dll", CharSet = CharSet.Auto,
         CallingConvention = CallingConvention.StdCall)]
        public static extern int SetWindowsHookEx(int idHook, HookProc lpfn,
        IntPtr hInstance, int threadId);
        
        [DllImport("user32.dll", CharSet = CharSet.Auto,
         CallingConvention = CallingConvention.StdCall)]
        public static extern bool UnhookWindowsHookEx(int idHook);
        
        [DllImport("user32.dll", CharSet = CharSet.Auto,
         CallingConvention = CallingConvention.StdCall)]
        public static extern int CallNextHookEx(int idHook, int nCode,
        IntPtr wParam, IntPtr lParam);
        public string pathstart = @"https://cdn.star.nesdis.noaa.gov/GOES16/ABI/FD/GEOCOLOR/";
        public string pathend = @"_GOES16-ABI-FD-GEOCOLOR-5424x5424.jpg";
        string lastsuccessful = "";
        string last = "";
        Bitmap bitmap = null;
        public bool isinmontion = false;
        PerformanceCounter cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        public int posx = 0;
        bool waslasthwnddesktop = false;
        public int posy = 0;
        public int lastmposx = 0;
        public int lastmposy = 0;
        public string forcelen(string str, string filler, int len)
        {
            while (str.Length < len)
            {
                str = filler + str;
            }
            return str;
        }
        public float zoom = 1.0f;
        private NotifyIcon tray;
        bool paused = false;
        public enum displaymodes
        {
            full=0,
            top=1,
            random=2
        };
        int displaymodecount = Enum.GetNames(typeof(displaymodes)).Length;
        public displaymodes displaymode = displaymodes.full;
        public OrbitalDesktop()
        {
            cpuCounter.NextValue();
            System.Threading.Thread.Sleep(1000);
            if (true || !Debugger.IsAttached)
            {
                RegistryKey registryKey = Registry.CurrentUser.OpenSubKey
                ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                registryKey.SetValue("Orbital Desktop", Application.ExecutablePath);
            }
            if (!File.Exists("settings.stt"))
                File.WriteAllText("settings.stt", displaymode.ToString());
            displaymode = (displaymodes)Enum.Parse(typeof(displaymodes), File.ReadAllText("settings.stt"));
            tray = new NotifyIcon();
            tray.Icon = Icon.FromHandle(Resources.ODImage.GetHicon());
            tray.ContextMenu = new ContextMenu(new MenuItem[]
            {
                new MenuItem("Pause", Exit),
                new MenuItem("Refresh", _Refresh),
                new MenuItem("Next Mode", NextMode),
                new MenuItem("Previous Mode", PreviousMode),
                new MenuItem("Exit", Exit),
            });
            tray.Visible = true;
            Refresh(null, null);
            timer.Interval = 60000;
            timer.Tick += Refresh;
            timer.Start();
        }
        void SetMode(displaymodes newmode)
        {
            displaymode = newmode;
            File.WriteAllText("settings.stt", displaymode.ToString());
            forcerefresh = true;
            Refresh(null, null);
        }
        void NextMode(object sender, EventArgs e)
        {
            displaymodes newmode;
            if (((int)displaymode) == displaymodecount - 1)
                newmode = (displaymodes)((0));
            else
                newmode = (displaymodes)(((int)displaymode) + 1);
            SetMode(newmode);
        }
        void PreviousMode(object sender, EventArgs e)
        {
            displaymodes newmode;
            if (((int)displaymode) == 0)
                newmode = (displaymodes)((displaymodecount - 1));
            else
                newmode = (displaymodes)(((int)displaymode) - 1);
            SetMode(newmode);
        }
        void Pause(object sender, EventArgs e)
        {
            (tray.ContextMenu.Container.Components[0] as MenuItem).Text = "Resume";
            (tray.ContextMenu.Container.Components[0] as MenuItem).Click -= Pause;
            (tray.ContextMenu.Container.Components[0] as MenuItem).Click += Resume;
            paused = true;
        }
        void Resume(object sender, EventArgs e)
        {
            paused = false;
            (tray.ContextMenu.Container.Components[0] as MenuItem).Text = "Pause";
            (tray.ContextMenu.Container.Components[0] as MenuItem).Click -= Resume;
            (tray.ContextMenu.Container.Components[0] as MenuItem).Click += Pause;
        }
        public bool IsUnderStress()
        {
            float cpuusage =  cpuCounter.NextValue();
            if(cpuusage == 0.0f)
                cpuusage = cpuCounter.NextValue();
            try
            {
                var counters = PerformanceCounterCategory.GetCategories()
        .SelectMany(x => x.GetCounters("")).Where(x => x.CounterName.Contains("GPU"));

                bool validgpuvalue = false;
                float gpuusage = 0.0f;
                foreach (var counter in counters)
                {
                    float nval = counter.NextValue();
                    if (nval > 0.0f && nval < 100.1f)
                    {
                        validgpuvalue = true;
                        gpuusage = nval;
                        break;
                    }
                }
                if (validgpuvalue)
                {
                    if (gpuusage > 50.0f)
                    {
                        System.Threading.Thread.Sleep(2000);
                        if (gpuusage > 50.0f)
                            return true;
                    }
                }
            }
            catch(Exception bullshit)
            {

            }
            if (cpuusage > 40.0f)
            {
                System.Threading.Thread.Sleep(2000);
                if (cpuusage > 40.0f)
                    return true;
            }
            return false;
        }
        bool forcerefresh = false;
        private void _Refresh(object sender, EventArgs e)
        {
            forcerefresh = true;
            Refresh(sender, e);
        }
        private void Refresh(object sender, EventArgs e)
        {
            if (!forcerefresh)
            {
                if (paused)
                    return;
                if (IsUnderStress())
                    return;
            }
            DateTime then = DateTime.Now;
            string current = "";
            then = then.AddHours(1);
            then = then.AddSeconds(-then.Second);
            then = then.AddMinutes(-then.Minute);
            int i = 0;
            string ending = "";
            bool setlast = true;
            while (i < 100)
            {
                if (i != 0)
                    then = then.AddMinutes(-15);
                current = then.Year.ToString();
                current += forcelen(then.DayOfYear.ToString(), "0", 3);
                ending = forcelen((then.Hour).ToString(), "0", 2);
                ending += forcelen((then.Minute).ToString(), "0", 2);
                if (current + ending == lastsuccessful && !forcerefresh)
                    break;
                string url = pathstart + current + ending + pathend;
                try
                {
                    WebClient client = new WebClient();
                    Stream stream = client.OpenRead(url);
                    if (bitmap != null)
                        bitmap.Dispose();
                    bitmap = new Bitmap(stream);
                    var dm = displaymode;
                    float zoomx = 1.0f;
                    float zoomy = 1.0f;
                    var rand = new Random();
                    if (dm == displaymodes.random)
                        dm = (displaymodes)(((uint)rand.Next()) % displaymodecount);
                    if (dm == displaymodes.full)
                        zoomx = zoomy = 1.0f;
                    else if (dm == displaymodes.top)
                        zoomy = 1.8f;
                    Bitmap shbitmap = bitmap.Clone(new Rectangle(0, 0, (int)((float)5424 / zoomx), (int)((float)5424 / zoomy)), bitmap.PixelFormat);
                    SetWallpaper(shbitmap);
                    shbitmap.Dispose();
                    bitmap.Dispose();
                    stream.Flush();
                    stream.Close();
                    client.Dispose();
                    GC.Collect();
                    setlast = false;
                    lastsuccessful = current + ending;
                    last = current + ending;
                    break;
                }
                catch (Exception ex)
                {
                    i++;
                    continue;
                }
                i++;
            }
            if (setlast)
            {
                ending = forcelen((then.Hour).ToString(), "0", 3);
                ending += forcelen((then.Minute).ToString(), "0", 3);
                last = current + ending;
            }
            forcerefresh = false;
        }
        void Exit(object sender, EventArgs e)
        {
            tray.Visible = false;

            Application.Exit();
        }
        public int SetWallpaper(Bitmap bmp)
        {
            string tempPath = Path.Combine(Path.GetTempPath(), "wallpaper.bmp");
            bmp.Save(tempPath, System.Drawing.Imaging.ImageFormat.Bmp);
            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop", true);
            key.SetValue(@"WallpaperStyle", 6.ToString());
            key.SetValue(@"TileWallpaper", 0.ToString());
            int res = SystemParametersInfo(SPI_SETDESKWALLPAPER,
                1,
                tempPath,
                SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
            int error = Marshal.GetLastWin32Error();
            GC.Collect();
            return error;
        }

    }
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new OrbitalDesktop());
        }
    }
}
